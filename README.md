#Update and upgrade the package

```
$apt-get update
$apt-get upgrade
```

#add a user and configure
```
$useradd deploy

$mkdir /home/deploy

$mkdir /home/deploy/.ssh

$chmod 700 /home/deploy/.ssh //chown is still needed for the user
```

#Setup the preferred shell
```
$usermod -s /bin/bash deploy
```

#copy the local directory id_rsa.pub  to your servers authorized key
```
$vim /home/deploy/.ssh/authorized_keys
```

#set the particular user permission
```
$chmod 400 /home/deploy/.ssh/authorized_keys

$chown deploy:deploy /home/deploy -R
```

#add a password to a user
```
passwd deploy
```

#configure sudo
```
$visudo

//add the line 
root    ALL=(ALL) ALL
%sudo   ALL=(ALL:ALL) ALL
```

#add deploy user to the sudo group
```
$usermod -aG sudo deploy
```

#check it by
```
$exec su -l deploy
```

#configure ssh file to permit passwordless login
```
$vim /etc/ssh/sshd_config

//add the line

PermitRootLogin no
PasswordAuthentication no
AllowUsers deploy@(your-VPN-or-static-IP)
```
#restart the service
```
$service ssh restart
```
##setting up the firewall
#install the firewall
```
$apt-get install ufw  //install the ufw
```

#open the config file of ufw set ipv6 to yes
```
$vim /etc/default/ufw
//make
IPV6 = yes
```

#run some command of ufw to allow the ports
```
$sudo ufw allow from {your-ip} to any port 22
$sudo ufw allow 80
$sudo ufw allow 443
$sudo ufw disable
$sudo ufw enable
```

#install fail2ban
```
$apt-get install fail2ban
```